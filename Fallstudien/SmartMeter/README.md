Fallstudie 

# Smart-Meter Rollout in Stäfa


Stäfa wird smart! 

(Aug. 2023)


Mit dem Ersatz der heutigen Stromzähler durch intelligente «Smart Meter» können wir in Zukunft Ihren Stromverbrauch effizient und kostengünstig aus der Ferne auslesen und quartalsweise effektive Verbrauchsrechnungen erstellen. 

Im Gegenzug erhalten Sie über unser Kundenportal Zugang zu Ihren aktuellen Energieverbrauchszahlen. 
Das schafft nicht nur Transparenz, sondern ermöglicht Optimierungen und Einsparungen, was in Zeiten steigender Energiepreise immer wichtiger wird. 

Bis die vorweihnächtlichen Besuche unserer Ableser der Vergangenheit angehören, müssen Sie sich aber noch etwas gedulden.

![gws-smartmeter-rollout](gws-smartmeter-rollout.png)


Homepage https://www.gws.ch

https://www.wetzikon.ch/politik/weitere-behoerden/werkkommission/beschluesse-der-werkkommission/2021/listingblock.2021-03-10.2959588864/wkb-2021-22-implementierung-smart-meter-system.pdf/@@download/file/WKB%202021-22%20Implementierung%20Smart%20Meter%20System%20inkl.%20Z%C3%A4hlerbeschaffung%20%28Beschaffung%29%2C%20Kreditbewilligung.pdf


https://www.vte.ch/wp-content/uploads/2019/08/2_Smart_Meter_180222.pdf


https://www.bfe.admin.ch/bfe/de/home/versorgung/stromversorgung/stromnetze/smart-grids.html


https://www.ecoplan.ch/download/smmu_sb_de.pdf
