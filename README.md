# M156 - Neue Services entwickeln und Einführung planen

[**Modulidentifikation** ICT CH](https://www.modulbaukasten.ch/module/156/2/de-DE?title=Neue-Services-entwickeln-und-Einf%C3%BChrung-planen)

## Unterlagen

- [Bücher](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/01-Buecher-Compendio?ref_type=heads)
- [ITIL](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/02-ITIL?ref_type=heads)
- [FitSM](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/03-FitSM?ref_type=heads)
- [BPMN](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/04-BPMN?ref_type=heads)
- [SLA](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Unterlagen/05-SLA?ref_type=heads)


[Fallstudien](https://gitlab.com/ch-tbz-it/Stud/m156/-/tree/main/Fallstudien?ref_type=heads)


## Bewertung / Modulnote

- LB1: 40% Lernprodukt "Service Management"
- LB2: 30% Offerte als Vorabgabe des IT-Service (aus Fallstudie)
- LB3: 30% Umsetzung des IT-Services inkl. Support usw.


Alle 3 Notenbestandteile sind in Gruppen zu 3, ausnahmsweise 2 Personen zu erarbeiten. **In der Regel** bekommen **alle im TEAM die gleiche Note**. Die Lehrperson behält sich vor, die Team-Arbeiten individuell zu bewerten, sodass im Team unterschiedliche Noten zustande kommen.




