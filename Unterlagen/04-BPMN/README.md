# BPMN - Business Process Modelling Notation


https://de.wikipedia.org/wiki/Business_Process_Model_and_Notation


![diagram.png](x-ressources/diagram.png)


http://www.bpmb.de/images/BPMN2_0_Poster_DE.pdf
<br>[x-ressources/BPMN2_0_Poster_DE.pdf](x-ressources/BPMN2_0_Poster_DE.pdf)


[x-ressources/eCH-BPMN-Modellierungskonventionen-fuer-die-oeffentliche-Verwaltung.pdf](x-ressources/eCH-BPMN-Modellierungskonventionen-fuer-die-oeffentliche-Verwaltung.pdf)


https://www.signavio.com/de/prozessmodellierung-mit-bpmn-2-0
<br>https://www.signavio.com/de/bpmn-einfuehrung



https://www.gbtec.com/de/ressourcen/bpmn-2-0


- [08:53 min, Einfache Modellierung in BPMN 2.0 - Schulung, YouTube, GBTEC Group](https://www.youtube.com/watch?v=xmls25KO7V8&t=144s)
- [09:11 min, Easy modeling in BPMN 2.0 - Tutorial, YouTube, GBTEC Group](https://www.youtube.com/watch?v=6a3HMjzKxGs)



https://signup.camunda.com/free-30-day-trial
